export default {
  state: {
    processing: false,
    processingCount: 0,
    processingOriginalCount: 0,
  },
  mutations: {
    setProcessing(state, processing) {
      state.processing = processing;
    },
    setProcessingCount(state, count) {
      state.processingCount = count;
    },
    setProcessingOriginalCount(state, count) {
      state.processingOriginalCount = count;
    },
  },
  actions: {
    addProcessing(context, { processing, count }) {
      context.commit('setProcessing', processing);
      context.commit('setProcessingCount', count);
      if (context.state.processingOriginalCount < count) {
        context.commit('setProcessingOriginalCount', count);
      }
    },
  },
  getters: {
    processing(state) {
      return state.processing;
    },
    processingCount(state) {
      return state.processingCount;
    },
    processingOriginalCount(state) {
      return state.processingOriginalCount;
    },
  },
};
